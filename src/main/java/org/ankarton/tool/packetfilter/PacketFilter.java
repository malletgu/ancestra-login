package org.ankarton.tool.packetfilter;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

public class PacketFilter {
	
	private int maxConnections, restrictedTime;
    private Map<String, IpInstance> ipInstances = new TreeMap<>();
    private Object locker = new Object();

    public PacketFilter(int maxConnections, int time, TimeUnit unit) {
		this.maxConnections = maxConnections;
		this.restrictedTime = (int)TimeUnit.MILLISECONDS.convert(time, unit);
	}

    public boolean check(String ip) {
        IpInstance ipInstance = find(ip);
		
		if (ipInstance.isBanned()) {
			return false;
		} else {
            ipInstance.incrementConnection();

            if (ipInstance.getLastConnection() + this.restrictedTime >= System.currentTimeMillis()) {
				if (ipInstance.getConnections() < this.maxConnections)
					return true;
				else {
					ipInstance.ban();
					return false;
				}
			} else {
				ipInstance.updateLastConnection();
				ipInstance.resetConnections();
			}
			return true;
		}
	}
	
	 private IpInstance find(String ip) {
         ip = ip.contains(":") ? ip.split(":")[0] : ip;
         synchronized (locker) {
             IpInstance result = ipInstances.get(ip);
             if (result != null) return result;
             result = new IpInstance();
             ipInstances.put(ip, result);
             return result;
         }
     }
}
