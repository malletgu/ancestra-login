package org.ankarton.object;

import org.ankarton.kernel.Main;
import org.ankarton.login.LoginClient;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Account {
    private final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

    private final int id;
    private final String name, pass, question, answer;
    private String pseudo;
	private final boolean enableConsole;
	private short state;
    private Calendar lastConnection;
    private InetAddress ip;
	private LoginClient client;
	private long subscribe;
	
	private Map<Short, Byte> players = new HashMap<>();

    public Account(int UUID, String name, String pass, String pseudo, String question, String answer,
                   boolean enableConsole, short state, String lastConnection, String ip, long subscribe, byte banned, String players) {
        this.id = UUID;
        this.name = name;
		this.pass = pass;
		this.pseudo = pseudo;
		this.question = question;
        this.answer = answer;
		this.enableConsole = enableConsole;
		this.state = (banned == 1 ? -1 : state);
		this.subscribe = subscribe;
        if(lastConnection.isEmpty())
            this.lastConnection = Calendar.getInstance();
        else {
            try {
                this.lastConnection = Calendar.getInstance();
                this.lastConnection.setTime(formatter.parse(lastConnection));
            } catch (ParseException e) {
                e.printStackTrace();
                this.lastConnection = Calendar.getInstance();
            }
        }

        try {
            this.ip = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            this.setEmptyIp();
        }

		if(!players.isEmpty()) {
			for(String arg : players.split(";")) {
				short server = Short.parseShort(arg.split(",")[0]);
				byte number = 	Byte.parseByte(arg.split(",")[1]);

				this.players.put(server, number);
			}
		}
    }

    public int getId() {
        return id;
    }
	
	public String getName() {
		return name;
	}
	
	public String getPass() {
		return pass;
	}
	
	public String getPseudo() {
		return pseudo;
	}
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
		Main.database.getAccountData().update(this);
	}
	
	public String getQuestion() {
		return question;
	}

    public String getAnswer() { return answer; }
	
	public boolean consoleAvailable() {
		return enableConsole;
	}
	
	public LoginClient getClient() {
		return client;
	}
	
	public void setClient(LoginClient client) {
		this.client = client;
	}
	
	public short getState() {
		return this.state;
	}

	public void setState(short state, boolean updateOnDatabase){
		this.state = state;
		if(updateOnDatabase)
			Main.database.getAccountData().update(this);
	}
	
	public void setState(short state) {
		setState(state,true);
	}

    public Calendar getLastConnection() {
        return lastConnection;
    }

    public void setEmptyIp() {
        try {
            this.ip = InetAddress.getByName("0.0.0.0");
        } catch (UnknownHostException e1) {}
    }

    public InetAddress getIp() {
        return ip;
    }

    public long getSubscribeRemaining() {
        long remaining = (subscribe - System.currentTimeMillis());
        return remaining<=0?0:remaining;
	}
	
	public long getSubscribe() {
		long remaining = (subscribe - System.currentTimeMillis());
		return remaining<=0?0:subscribe;
	}

	public Map<Short, Byte> getPlayers() { return players; }

    public void connect() {
        try {
            this.ip = InetAddress.getByName(client.getSession().getRemoteAddress().toString().replace("/", "").split(":")[0]);
        } catch (UnknownHostException e) {
            this.setEmptyIp();
        }
        this.lastConnection = Calendar.getInstance();
    }

	public String parsePlayers() {
		String players = "";
		
		for(Map.Entry<Short, Byte> server : this.players.entrySet())
			players += (players.isEmpty() ? server.getKey() + "," + server.getValue() : ";" + server.getKey() + "," + server.getValue());
		return players;
	}
}
