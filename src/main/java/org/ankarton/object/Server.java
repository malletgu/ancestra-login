package org.ankarton.object;

import org.ankarton.exchange.ExchangeClient;
import org.ankarton.login.LoginHandler;
import org.apache.mina.core.buffer.IoBuffer;

import java.util.HashMap;
import java.util.Map;

public class Server {
    public enum State {
        OFFLINE((byte) 0),
        ONLINE((byte) 1),
        SAVING((byte) 2);

        public final byte id;

        private State(byte id) {
            this.id = id;
        }
    }

    public enum Population {
        RECOMMENDED((byte) 0, 1000),
        AVERAGE((byte) 1, 500),
        HIGH((byte) 2, 300),
        LOW((byte) 3, 200),
        FULL((byte) 4, 20);
        public final byte id;
        public final int seuil;

        private Population(byte id, int seuil) {
            this.id = id;
            this.seuil = seuil;
        }
    }


    public static Map<Short, Server> servers = new HashMap<>();

    public static Server get(short id) {
        return servers.containsKey(id) ? servers.get(id): null;
    }

    public static void sendHostListToAll() {
        LoginHandler.sendToAll(getHostList());
    }


    public static String getHostList() {
        StringBuilder sb = new StringBuilder("AH");
        boolean first = true;

        for(Server server : servers.values()) {
            sb.append(first ? "" : "|").append(server.getId()).append(";")
                    .append(server.getState().id).append(";")
                    .append(server.getPopulation().id).append(";")
                    .append(server.subscriptionNeeded? 1 : 0);
            first = false;
        }

        return sb.toString();
    }

    private short id;
    private int port;
    private final boolean subscriptionNeeded;
    private Population population;
    private State state;
    private String ip;
    private long key;

    private ExchangeClient client;

    public Server(short id, long key, byte population, boolean subscribeNeeded) {
        this.id = id;
        this.key = key;
        this.state = State.OFFLINE;
        this.population = Population.values()[population + 1];
        this.subscriptionNeeded = subscribeNeeded;

        servers.put(this.id, this);
    }

    public short getId() {
        return id;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
        sendHostListToAll();
    }

    public long getKey() {
        return key;
    }

    public boolean isSubscriptionNeeded() {
        return subscriptionNeeded;
    }

    public ExchangeClient getClient() {
        return client;
    }

    public void setClient(ExchangeClient client) {
        this.client = client;
    }

    public void setPopulation(int freePlaces) {
        for (Population population : Population.values()) {
            if (freePlaces > population.seuil) {
                this.population = population;
                return;
            }
        }
    }

    public Population getPopulation() {
        return population;
    }

    public void send(IoBuffer arg0) {
        arg0.flip();
        this.getClient().getSession().write(arg0);
    }
}
