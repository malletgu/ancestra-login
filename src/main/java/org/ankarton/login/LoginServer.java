package org.ankarton.login;

import ch.qos.logback.classic.Logger;
import org.ankarton.kernel.Main;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class LoginServer {

    public static ArrayList<LoginClient> clients = new ArrayList<>();
    private static NioSocketAcceptor acceptor;

    public LoginServer() {
        acceptor = new NioSocketAcceptor();
        acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(
                new TextLineCodecFactory(Charset.forName("UTF8"), LineDelimiter.NUL, new LineDelimiter("\n\0"))));
        acceptor.setHandler(new LoginHandler());
    }

    public void start() {
        if (acceptor.isActive())
            return;
        Logger logger = (Logger) LoggerFactory.getLogger("org.apache.mina");

        try {
            acceptor.bind(new InetSocketAddress(Main.networkConfig.loginIp(), Main.networkConfig.loginPort()));
            logger.info("Login server started on port " + Main.networkConfig.loginPort() + ".");
        } catch (IOException e) {
            logger.error("Fail to bind acceptor : " + e);
            try { Thread.sleep(3000); }catch(Exception ignored) {}
            this.start();
        }
    }

    public void stop() {
        if (!acceptor.isActive())
            return;

        acceptor.unbind();
        acceptor.getManagedSessions().values().stream().parallel().filter(session -> session.isConnected() || !session.isClosing()).forEach(session -> session.close(true));
        acceptor.dispose();

        Logger logger = (Logger) LoggerFactory.getLogger("org.apache.mina");
        logger.info("Login server stopped.");
    }

    public static boolean isAvailable() {
        return acceptor.isActive();
    }
}
