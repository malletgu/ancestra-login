package org.ankarton.login.packet;

import org.ankarton.exchange.packet.PacketHandler;
import org.ankarton.login.LoginClient;
import org.ankarton.object.Account;
import org.ankarton.object.Server;
import org.apache.mina.core.buffer.IoBuffer;

/**
 * @author Locos & Erfive
 */
public class AccountQueue {

    /**
     * AlEa : Deja en connexion. Veuillez reessayer
     * AlEb : Connexion refusee. Ton compte a ete banni.
     * AlEc : Ce compte est deja connecte a un serveur de jeu. Veuillez reessayer.
     * AlEd : Tu viens de deconnecter un personnage utilisant deja ce compte.
     * AlEe : ATTENTION : Vous devez utilisez vos identifiants ANkama Games ! ...
     * AlEf : Connexion refusee. Nom de compte ou mot de passe incorrect.
     */

	public static void verify(LoginClient client) {
		Account account = client.getAccount();
		short state = account.getState();
        switch (state) {
			case -1: //banned
				client.send("AlEb");
				client.kick();
                client.logger.debug("is banned.");
				break;

			case 0: //disconnected
                sendInformation(client);
				break;

			default:// in game
				if(state > 0) {
					IoBuffer buffer = IoBuffer.allocate(5);
					buffer.clear();
					buffer.put(PacketHandler.KICK_ACCOUNT);
					buffer.putInt(account.getId());
                    Server server = Server.get(state);
                    if(server != null)
					    server.send(buffer);
				}
				account.setState((short) 0);
				client.send("AlEd");
				client.kick();
                client.logger.debug("is already connected.");
				break;
        }
    }

	public static void sendInformation(LoginClient client) {
		Account account = client.getAccount();

		if(account.getPseudo().isEmpty()) {
			client.send("AlEr");
			client.setStatus(LoginClient.Status.WAIT_NICKNAME);
            client.logger.debug("is waiting for nickname.");
			return;
		}

        client.send("Af0|0|0|1|-1");
        client.send("Ad" + account.getPseudo());
		client.send("Ac0");
		client.send(Server.getHostList());
		client.send("AlK" + (account.consoleAvailable() ? 1 : 0));
		client.send("AQ" + account.getQuestion());
        client.logger.debug("is in selection of server.");
	}
}
