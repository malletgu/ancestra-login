package org.ankarton.login.packet;

import org.ankarton.kernel.Main;
import org.ankarton.login.LoginClient;
import org.ankarton.object.Account;
import org.ankarton.object.Server;

public class FriendServerList {
	
	public static void get(LoginClient client, String packet) {
		try {
            String name = Main.database.getAccountData().getNameFromPseudo(packet);

            if (name == null) {
                client.send("AF");
                return;
			}
			
			Account account = Main.database.getAccountData().load(name);
			
			if(account == null) {
				client.send("AF");
				return;
			}

			client.send("AF" + getList(account));
		} catch(Exception e) {
            e.printStackTrace();
        }
	}

    private static String getList(Account account) {
        StringBuilder sb = new StringBuilder();

        for (Server server : Server.servers.values()) {
			if(server == null) continue;
			if(account.getPlayers().get(server.getId()) == null) continue;

            int i = account.getPlayers().get(server.getId());
			if(i != 0)
				sb
				.append(server.getId()).append(",")
				.append(i).append(";");
		}
		return sb.toString();
	}
}
