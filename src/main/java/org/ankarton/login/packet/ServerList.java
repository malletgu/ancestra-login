package org.ankarton.login.packet;

import org.ankarton.login.LoginClient;
import org.ankarton.object.Account;
import org.ankarton.object.Server;

public class ServerList {
	
	public static void get(LoginClient client) {
		client.send("AxK" + serverList(client.getAccount()));
	}
	
	public static String serverList(Account account) {
		StringBuilder sb = new StringBuilder(String.valueOf(account.getSubscribeRemaining()));
		
		for(Server server : Server.servers.values()) {
			if(account.getPlayers().get(server.getId()) == null)
				continue;

			int i = account.getPlayers().get(server.getId());

			if(i < 1)
				continue;
			
			sb
			.append("|").append(server.getId())
			.append(",").append(i);
		}
		return sb.toString();
	}
}
