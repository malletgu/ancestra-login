package org.ankarton.login.packet;

import org.ankarton.database.data.AccountData;
import org.ankarton.login.LoginClient;
import org.ankarton.login.LoginClient.Status;

public class AccountName {
	
	public static void verify(LoginClient client, String name) {
		try {
			client.setAccount(AccountData.get(name.toLowerCase()));
		} catch(Exception e) {
			client.send("AlEf");
			client.kick();
			return;
		}
		
		if(client.getAccount() == null) { 
			client.send("AlEf");
			client.kick();
			return;
		}

		client.setStatus(Status.WAIT_PASSWORD);
        client.logger.debug("waiting for password.");
	}
}
