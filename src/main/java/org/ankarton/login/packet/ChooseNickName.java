package org.ankarton.login.packet;

import org.ankarton.kernel.Main;
import org.ankarton.login.LoginClient;
import org.ankarton.object.Account;


public class ChooseNickName {
	
	public static void verify(LoginClient client, String nickname) {
		Account account = client.getAccount();
		
		if(!account.getPseudo().isEmpty()) {
			client.kick();
			return;
		}
		
		if(nickname.toLowerCase().equals(account.getName().toLowerCase())) {
			client.send("AlEr");
			return;
		}

		String s[] = {"admin", "modo", " ", "&", "�", "\"", "'", 
				"(", "-", "�", "_", "�", "�", ")", "=", "~", "#",
				"{", "[", "|", "`", "^", "@", "]", "}", "�", "+",
				"^", "$", "�", "*", ",", ";", ":", "!", "<", ">",
				"�", "�", "%", "�", "?", ".", "/", "�", "\n"};

        for (String value : s) {
            if (nickname.contains(value)) {
                client.send("AlEs");
                break;
            }
        }

        if (Main.database.getAccountData().existPseudo(nickname)) {
            client.send("AlEs");
            client.logger.debug("nickname '" + nickname + "' is already used.");
            return;
        }

		client.getAccount().setPseudo(nickname);
		client.setStatus(LoginClient.Status.SERVER);
		AccountQueue.verify(client);
	}
}
