package org.ankarton.login.packet;

import org.ankarton.kernel.Main;
import org.ankarton.kernel.config.DatabaseConfig;
import org.ankarton.login.LoginClient;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Password {
	
	public static char[] hash = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A',
			'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4',
			'5', '6', '7', '8', '9', '-', '_'};
	
	public static void verify(LoginClient client, String pass) {
		boolean success = Main.databaseConfig.isCrypted()?
                !cryptTo(
                        Password.cryptTo(
                                Password.decryptPassword(pass, client.getKey()), "MD5"), "SHA-512")
                        .equals(client.getAccount().getPass()):pass.equals(client.getAccount().getPass());
        if(success){
			client.send("AlEf");
			client.kick();
			return;
		}

		if(client.getAccount().getClient() != null)
			client.getAccount().getClient().kick();

		client.getAccount().setClient(client);
		client.setStatus(LoginClient.Status.SERVER);
	}
	
	public static String cryptPassword(LoginClient client) {
		String pass = client.getAccount().getPass();
		String key = client.getKey();
		int i = hash.length;
		
		StringBuilder crypted = new StringBuilder();
        
		for(int y = 0; y < pass.length(); y++) {
			char c1 = pass.charAt(y);
            char c2 = key.charAt(y);
            double d = Math.floor(c1 / 16);
            int j = c1 % 16;
            
            crypted.append(hash[(int) ((d + c2 % i) % i)])
            .append(hash[(j + c2 % i) % i]);
		}
        
		return crypted.toString();
	}

	public static String decryptPassword(String pass, String key) {
		int i1 = 0, i2 = 0;
		String decrypted = "";

		for (int i = 0; i < pass.length(); i += 2) {
			char c1 = key.charAt(i / 2);

			for(byte y = 0; y < hash.length; y++) {
				if(hash[y] == pass.charAt(i)) {
					i1 = (y + hash.length) - (int) c1;
					if (i1 < 0) i1 += 64;
					i1 *= 16;
					break;
				}
			}

			for(byte y = 0; y < hash.length; y++) {
				if(hash[y] == pass.charAt(i + 1)) {
					i2 = (y + hash.length) - (int) c1;
					if (i2 < 0) i1 += 64;
					break;
				}
			}

			decrypted += (char) (i1 + i2);
		}

		return decrypted;
	}

	private static String cryptTo(String msg, String type) {
		try {
			MessageDigest digest = MessageDigest.getInstance(type);
			digest.update(msg.getBytes());
			byte[] bytes = digest.digest();
			String out = "";

			for (byte b : bytes) {
				String s = Integer.toHexString(b);
				while (s.length() < 2) {
					s = "0" + s;
				}
				s = s.substring(s.length() - 2);
				out += s;
			}

			return out;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
}
