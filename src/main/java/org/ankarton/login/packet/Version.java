package org.ankarton.login.packet;

import org.ankarton.kernel.Main;
import org.ankarton.login.LoginClient;

public class Version {
	
	public static void verify(LoginClient client, String version) {
		if(!version.equalsIgnoreCase(Main.gameConfig.version())) {
			client.send("AlEv" + Main.gameConfig.version());
			client.kick();
            client.logger.debug("invalid version.");
            return;
		}
		
		client.setStatus(LoginClient.Status.WAIT_ACCOUNT);
        client.logger.debug("is waiting for account name.");
	}
}
