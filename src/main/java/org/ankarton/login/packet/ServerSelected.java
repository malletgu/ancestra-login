package org.ankarton.login.packet;

import org.ankarton.exchange.packet.PacketHandler;
import org.ankarton.kernel.Main;
import org.ankarton.kernel.config.GameConfig;
import org.ankarton.login.LoginClient;
import org.ankarton.object.Account;
import org.ankarton.object.Server;
import org.apache.mina.core.buffer.IoBuffer;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

public class ServerSelected {

    public static void get(LoginClient client, String packet) {
        Server server;

        try {
            short i = Short.parseShort(packet);
            server = Server.get(i);
        } catch (Exception e) {
            client.send("AXEr");
            client.kick();
            e.printStackTrace();
            return;
        }

        if (server == null) {
            client.send("AXEr");
            return;
        }

        if (server.getState() != Server.State.ONLINE) {
            client.send("AXEd");
            return;
        }

        final Account account = client.getAccount();

        if (account.getSubscribeRemaining() == 0 && server.isSubscriptionNeeded()) {
            client.send(getFreeServer());
            return;
        }

        IoBuffer buffer1 = IoBuffer.allocate(37 + account.getPseudo().length());
        buffer1.clear();
        buffer1.put(PacketHandler.AUTHORIZE_NEW_ACCOUNT);
        buffer1.putInt(account.getId());
        buffer1.putInt(((InetSocketAddress) client.getSession().getRemoteAddress()).getAddress().getHostAddress().hashCode());
        buffer1.putLong(System.currentTimeMillis() + Main.networkConfig.authorizedConnectionDelay() * 1000);
        buffer1.putLong(account.getSubscribeRemaining());
        buffer1.putLong(account.getLastConnection().getTimeInMillis());
        buffer1.put(account.getIp().getAddress());
        buffer1.put(Charset.forName("UTF-8").encode(account.getPseudo()));
        server.send(buffer1);

        account.setState(server.getId());
        account.connect();

        StringBuilder sb = new StringBuilder("AYK");
        sb.append(server.getIp()).append(":")
            .append(server.getPort()).append(";")
            .append(account.getId());

        client.send(sb.toString());
        client.logger.debug("has choose server " + server.getId() + ".");
    }

    private static String getFreeServer() {
        StringBuilder sb = new StringBuilder("AXEf");
        boolean first = true;

        for (Server server : Server.servers.values()) {
            if (!server.isSubscriptionNeeded() && server.getPopulation() != Server.Population.FULL) {
                sb.append(first ? "" : "|").append(server.getId());
                first = false;
            }
        }
    
        return sb.toString();
    }
}
