package org.ankarton.login;

import ch.qos.logback.classic.Logger;
import org.ankarton.login.packet.PacketHandler;
import org.ankarton.object.Account;
import org.apache.mina.core.session.IoSession;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class LoginClient {

    public enum Status {
        WAIT_VERSION,
        WAIT_PASSWORD,
        WAIT_ACCOUNT,
        WAIT_NICKNAME,
        SERVER
    }

    private final String key;
    private final IoSession ioSession;
    private Status status;
    private Account account;
    public final Logger logger;

    public LoginClient(IoSession ioSession) {
        this.ioSession = ioSession;
        this.key = generateKey();
        this.logger = (Logger) LoggerFactory.getLogger("lSession" + ioSession.getId());
        this.logger.debug("has been created.");
    }

    public IoSession getSession() {
        return ioSession;
    }

    public String getKey() {
        return key;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void send(Object object) {
        this.ioSession.write(object);
    }

    void parser(String packet) {
        PacketHandler.parser(this, packet);
    }

    public void kick() {
        if(LoginServer.clients.contains(this))
            LoginServer.clients.remove(this);
        if(!this.ioSession.isClosing())
            this.ioSession.close();
    }

    private static String generateKey() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder hashKey = new StringBuilder();
        Random rand = new Random();

        for (int i = 0; i < 32; i++)
            hashKey.append(alphabet.charAt(rand.nextInt(alphabet.length())));
        return hashKey.toString();
    }
}
