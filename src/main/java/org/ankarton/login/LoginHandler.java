package org.ankarton.login;

import org.ankarton.login.LoginClient.Status;
import org.ankarton.tool.packetfilter.PacketFilter;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import java.util.concurrent.TimeUnit;

public class LoginHandler implements IoHandler {

    private static PacketFilter filter = new PacketFilter(5, 1, TimeUnit.SECONDS);

    @Override
    public void exceptionCaught(IoSession arg0, Throwable arg1)
            throws Exception {
        LoginClient client = (LoginClient) arg0.getAttribute("loginClient");

        if(arg1 instanceof java.io.IOException)
            client.logger.error("exception : The connexion with the host has been closed.");
        else
            client.logger.error("exception : {}", arg1);

        try {
            client.kick();
        } catch (Exception ignored){}
    }

    @Override
    public void messageReceived(IoSession arg0, Object arg1) throws Exception {
        LoginClient client = (LoginClient) arg0.getAttribute("loginClient");
        String packet = (String) arg1;
        String[] s = packet.split("\n");
        int i = 0;
        do {
            client.logger.trace("received : " + s[i]);
            client.parser(s[i]);
            i++;
        } while (i == s.length - 1);
    }

    @Override
    public void messageSent(IoSession arg0, Object arg1) throws Exception {
        LoginClient client = (LoginClient) arg0.getAttribute("loginClient");
        client.logger.trace("sent : " + arg1);
    }

    @Override
    public void sessionClosed(IoSession arg0) throws Exception {
        LoginClient client = (LoginClient) arg0.getAttribute("loginClient");
        client.logger.debug("has been closed.");
        if(LoginServer.clients.contains(client))
            LoginServer.clients.remove(client);
    }

    @Override
    public void sessionCreated(IoSession arg0) throws Exception {
        if (!filter.check(arg0.getRemoteAddress().toString().substring(1).split(":")[0]))
            arg0.close(true);
        else {
            LoginClient client = new LoginClient(arg0);
            LoginServer.clients.add(client);
            arg0.setAttribute("loginClient", client);
            client.send("HC" + client.getKey());
            client.setStatus(Status.WAIT_VERSION);
        }
    }

    @Override
    public void sessionIdle(IoSession arg0, IdleStatus arg1) throws Exception {
        LoginClient client = (LoginClient) arg0.getAttribute("loginClient");
        client.logger.debug("has been idled.");
    }

    @Override
    public void sessionOpened(IoSession arg0) throws Exception {
        LoginClient client = (LoginClient) arg0.getAttribute("loginClient");
        client.logger.debug("has been opened.");
    }

    public static void sendToAll(String packet) {
        LoginServer.clients.stream().parallel().forEach(handler -> handler.send(packet));
    }
}
