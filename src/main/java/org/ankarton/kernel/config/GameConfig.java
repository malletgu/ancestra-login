package org.ankarton.kernel.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

/**
 * Created by guillaume on 18/09/14.
 */

@Config.Sources({
        "file:./config.properties",
        "classpath:./config.properties"
})
public interface GameConfig extends Reloadable {
    @Config.Key("game.dofus_version")
    @Config.DefaultValue("1.29.1")
    public String version();
}
