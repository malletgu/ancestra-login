package org.ankarton.kernel.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

/**
 * Created by guillaume on 18/09/14.
 */

@Config.Sources({
        "file:./config.properties",
        "classpath:./config.properties"
})
public interface NetworkConfig extends Reloadable {

    @Key("login.ip")
    @DefaultValue("127.0.0.1")
    public String loginIp();

    @DefaultValue("443")
    @Key("login.port")
    public int loginPort();

    @DefaultValue("127.0.0.1")
    @Key("exchange.ip")
    public String exchangeIp();

    @Key("exchange.port")
    @DefaultValue("6666")
    public int exchangePort();

    @Key("authorizedConnectionDelay")
    @DefaultValue("10000")
    int authorizedConnectionDelay();

}
