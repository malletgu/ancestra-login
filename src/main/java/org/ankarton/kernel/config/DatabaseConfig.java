package org.ankarton.kernel.config;

import ch.qos.logback.core.joran.spi.DefaultClass;
import org.aeonbits.owner.Config;

/**
 * Created by guillaume on 18/09/14.
 */


@Config.Sources({
        "file:./config.properties",
        "classpath:./config.properties"
})
public interface DatabaseConfig extends Config {

    @DefaultValue("127.0.0.1")
    @Key("database.host")
    String databaseHost();

    @DefaultValue("3306")
    @Key("database.port")
    int databasePort();

    @DefaultValue("ae_login")
    @Key("database.table_name")
    String databaseName();

    @DefaultValue("root")
    @Key("database.user")
    String databaseUser();

    @DefaultValue("")
    @Key("database.password")
    String databasePassword();

    @Config.DefaultValue("false")
    @Config.Key("database.cryptedPass")
    boolean isCrypted();


}
