package org.ankarton.kernel;

public enum EmulatorInfos {
	DEVELOPER("Locos & Erfive"),
    RELEASE(1.0),
    SOFT_NAME("Ankarton Login v"+RELEASE.value),
    HARD_NAME(SOFT_NAME + " for dofus " + Main.gameConfig.version() + " by " + DEVELOPER);
	
	private String string;
	private double value;
	
	private EmulatorInfos(String s) {
		this.string = s;
	}
	
	private EmulatorInfos(double d) {
		this.value = d;
	}

    @Override
	public String toString() {
		return this.string;
	}
}
