package org.ankarton.kernel;

import org.ankarton.login.LoginClient;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

/**
 * Created by Locos on 23/06/2015.
 */
public class CommandLinePrompt {

    private final static Scanner scanner = new Scanner(System.in);
    private final static CliTree based = new CliTree();

    static {
        based.addEntry("exit", "Close the server.", new Action() {
            @Override
            public void execute(Object caster, CliTree cliTree, String[] params) {
                Main.gracefullyExit();
            }
        });

    }

    public static void read(LoginClient client, String msg) {
        based.accept(client, msg.split(" "));
    }

    public static void read() {
        if(scanner.hasNextLine()) {
            String[] split = scanner.nextLine().split(" ");
            based.accept(null, split);
        }
    }

    public static class CliTree {
        private static Action helpConsumer = new Action() {
            @Override
            public void execute(Object caster, CliTree cliTree, String[] params) {
                cliTree.subEntries.forEach(t -> System.out.println(t.help));
            }
        };

        private final String keyword;
        private final String help;
        private final List<CliTree> subEntries = new LinkedList<>();
        private final Action action;

        public CliTree() {
            this.keyword = "";
            this.help = "";
            this.action = helpConsumer;
        }

        public void addEntry(String keyword, String help) {
            this.addEntry(keyword, help, helpConsumer);
        }

        public void addEntry(String keyword, String help, Action action) {
            this.subEntries.add(new CliTree(keyword, help, action));
        }

        private CliTree(String keyword, String help, Action action) {
            this.keyword = keyword;
            this.help = help;
            this.action = action;
        }

        public void accept(Object object, String[] words) {
            CliTree current = this;
            for(String word : words){
                Optional<CliTree> c = current.subEntries.stream().filter(t -> word.equals(t.keyword)).findFirst();
                if(!c.isPresent()) {
                    //Or just displaying help
                    current.action.execute(object, current, words);
                    return;
                } else
                    current = c.get();
            }
            current.action.execute(object, current, words);
        }
    }

    private static abstract class Action {
        public abstract void execute(Object caster, CliTree cliTree, String[] params);

        public void send(Object caster, String msg) {
            if(caster == null) {
                System.out.println(msg);
            } else {
                ((LoginClient) caster).send("BAT2" + msg);
            }
        }
    }
}
