package org.ankarton.kernel;

import ch.qos.logback.classic.Logger;
import org.aeonbits.owner.ConfigFactory;
import org.ankarton.database.Database;
import org.ankarton.exchange.ExchangeServer;
import org.ankarton.kernel.config.DatabaseConfig;
import org.ankarton.kernel.config.GameConfig;
import org.ankarton.kernel.config.NetworkConfig;
import org.ankarton.login.LoginServer;
import org.nfunk.jep.function.Random;
import org.slf4j.LoggerFactory;

public class Main implements Runnable {

    private static final Thread finalThread;

    public static NetworkConfig networkConfig;
    public static DatabaseConfig databaseConfig;
    public static GameConfig gameConfig;

	static {
        System.setProperty("logback.configurationFile", "logback.xml");
        finalThread = new Thread(new Main());
        finalThread.setDaemon(false);
        Runtime.getRuntime().addShutdownHook(finalThread);
        networkConfig = ConfigFactory.create(NetworkConfig.class);
        databaseConfig = ConfigFactory.create(DatabaseConfig.class);
        gameConfig = ConfigFactory.create(GameConfig.class);
    }

    private static Logger logger = (Logger) LoggerFactory.getLogger(Main.class);
    private static LoginServer loginServer;
    private static ExchangeServer exchangeServer;
    public static Database database = new Database();

	public static void main(String[] arg) {
        start();
	}

	public static void start() {
        logger.debug(EmulatorInfos.HARD_NAME.toString());

        Main.database.initializeConnection();
        Main.database.initializeData();

        logger.info("Creation of the connexion server.");
        long startTime = System.currentTimeMillis();
        Main.database.getServerData().loadAll();

        logger.info("The connexion server was started in : " + (System.currentTimeMillis() - startTime) + " ms.");

        exchangeServer = new ExchangeServer();
        exchangeServer.start();
        loginServer = new LoginServer();
        loginServer.start();

        while (LoginServer.isAvailable()) {
            CommandLinePrompt.read();

            try {
                Thread.sleep(10);
            } catch (Exception ignored) {}
        }
    }

    public static void gracefullyExit() {
        Runtime.getRuntime().removeShutdownHook(finalThread);
        finalThread.run();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            logger.error("Error while shutting down", e);
        }
        System.exit(5);
    }

    @Override
    public void run() {
        logger.info("Closing the connexion server");

        if(loginServer != null)
            loginServer.stop();
        if(exchangeServer != null)
            exchangeServer.stop();
        logger.debug("All of connexion servers have been closed.");
    }
}