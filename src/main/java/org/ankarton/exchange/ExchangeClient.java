package org.ankarton.exchange;

import ch.qos.logback.classic.Logger;
import org.ankarton.exchange.packet.PacketHandler;
import org.ankarton.object.Server;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.slf4j.LoggerFactory;

public class ExchangeClient {

	private final IoSession ioSession;
	private Server server;
	public final Logger logger;

	public ExchangeClient(IoSession ioSession) {
		this.ioSession = ioSession;
		this.logger = (Logger)LoggerFactory.getLogger("eSession" + ioSession.getId());
        this.logger.debug("has been created");
	}

	public IoSession getSession() {
		return ioSession;
	}
	
	public Server getServer() {
		return server;
	}
	
	public void setServer(Server server) {
		this.server = server;
	}
		
	public void send(IoBuffer buffer) {	this.ioSession.write(buffer); }
	
	void parser(IoBuffer packet) {
		PacketHandler.parser(this, packet);
	}
	
	public void kick() {
		this.ioSession.close(true);
	}
}
