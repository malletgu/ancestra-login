package org.ankarton.exchange;

import ch.qos.logback.classic.Logger;
import org.ankarton.kernel.Main;
import org.apache.mina.transport.socket.SocketAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;

public class ExchangeServer {

    SocketAcceptor acceptor = new NioSocketAcceptor();

    public ExchangeServer() {
        acceptor.setReuseAddress(true);
        acceptor.setHandler(new ExchangeHandler());
    }

    public void start() {
        if (acceptor.isActive())
            return;
        Logger logger = (Logger) LoggerFactory.getLogger("org.apache.mina");
        try {
            acceptor.bind(new InetSocketAddress(Main.networkConfig.exchangeIp(), Main.networkConfig.exchangePort()));
            logger.info("Exchange server started on address {}:{}. ",Main.networkConfig.exchangeIp(),Main.networkConfig.exchangePort());
        } catch (IOException e) {
            logger.error("Fail to bind acceptor : " + e);
            try { Thread.sleep(3000); }catch(Exception ignored) {}
            this.start();
        }
    }

    public void stop() {
        if (!acceptor.isActive())
            return;
        acceptor.unbind();
        acceptor.getManagedSessions().values().stream().parallel().filter(session -> session.isConnected() || !session.isClosing()).forEach(session -> session.close(true));
        acceptor.dispose();

        Logger logger = (Logger) LoggerFactory.getLogger("org.apache.mina");
        logger.info("Exchange server stopped.");
    }
}
