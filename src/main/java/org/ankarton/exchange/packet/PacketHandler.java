package org.ankarton.exchange.packet;

import org.ankarton.database.data.AccountData;
import org.ankarton.exchange.ExchangeClient;
import org.ankarton.object.Account;
import org.ankarton.object.Server;
import org.apache.mina.core.buffer.IoBuffer;

import java.net.Inet4Address;
import java.nio.charset.Charset;

public class PacketHandler {

    //region opcode
    public static final byte AUTHENTICATION = 1
            ,POPULATION = 2, SERVER_INFORMATION = 3, STATE = 4
            ,AUTHENTICATION_SUCCESS = 11, AUTHENTICATION_FAILURE = 10, ANSWER_CONFIRMATION = 12
            ,UPDATE_CHARACTER_NUMBER = 15, ACCOUNT_CONNECTED = 16, ACCOUNT_DISCONNECTED = 17
            ,AUTHORIZE_NEW_ACCOUNT = 30, KICK_ACCOUNT = 31
            ,ACTION_SUCCESS = 90, ACTION_FAIL = 91;
    //end region

    public static void parser(ExchangeClient client, IoBuffer packet) {
        try {
            IoBuffer answer = IoBuffer.allocate(20);
            answer.clear();

            switch (packet.get()) {
                case AUTHENTICATION:
                    short id = packet.getShort();
                    long key = packet.getLong();

                    Server server = Server.get(id);

                    if(server == null || Long.compare(server.getKey(), key) != 0) {
                        answer.put(AUTHENTICATION_FAILURE);
                        answer.flip();
                        client.send(answer);
                        client.kick();
                        return;
                    }

                    server.setClient(client);
                    client.setServer(server);

                    answer.put(AUTHENTICATION_SUCCESS);
                    answer.flip();
                    client.send(answer);
                    break;

                case SERVER_INFORMATION:
                    byte[] ip = new byte[4];
                    packet.get(ip);

                    String address = Inet4Address.getByAddress(ip).getHostAddress();
                    int port = packet.getInt();

                    client.getServer().setIp(address);
                    client.getServer().setPort(port);
                    answer.put(POPULATION);
                    answer.flip();
                    client.send(answer);
                    break;

                case POPULATION:
                    int freePlaces = packet.getInt();
                    client.getServer().setPopulation(freePlaces);
                    break;

                case STATE:
                    byte state = packet.get();
                    if(client.getServer() != null)
                        client.getServer().setState(Server.State.values()[state]);
                    break;

                case UPDATE_CHARACTER_NUMBER:
                    int uid = packet.getInt();
                    byte quantity = packet.get();
                    Account account = AccountData.get(uid);

                    if(account == null) {
                        answer.put(ACTION_FAIL);
                        answer.flip();
                        client.send(answer);
                        return;
                    }

                    if(account.getPlayers().get(client.getServer().getId()) != null)
                        account.getPlayers().remove(client.getServer().getId());

                    account.getPlayers().put(client.getServer().getId(), quantity);

                    answer.put(ACTION_SUCCESS);
                    answer.putInt(account.getId());
                    answer.flip();
                    client.send(answer);
                    break;

                case ACCOUNT_CONNECTED:
                    uid = packet.getInt();
                    AccountData.get(uid).setState(client.getServer().getId());
                    break;

                case ACCOUNT_DISCONNECTED:
                    uid = packet.getInt();
                    AccountData.get(uid).setState((short) 0);
                    break;

                case ANSWER_CONFIRMATION:
                    uid = packet.getInt();
                    account = AccountData.get(uid);

                    if(account != null) {
                        byte[] bytes = new byte[answer.capacity() - 5];
                        answer.get(bytes);
                        String answers = new String(bytes, Charset.forName("UTF-8"));

                        if(answers.equals(account.getAnswer().replace(' ', '+'))) {
                            answer.put(ACTION_SUCCESS);
                            answer.flip();
                            client.send(answer);
                            break;
                        }
                    }

                    answer.put(ACTION_FAIL);
                    answer.flip();
                    client.send(answer);
                    break;

                default:
                    client.kick();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            client.kick();
        }
    }
}
