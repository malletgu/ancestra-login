package org.ankarton.exchange;

import org.ankarton.kernel.Main;
import org.ankarton.object.Server;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

public class ExchangeHandler implements IoHandler {

    @Override
    public void exceptionCaught(IoSession arg0, Throwable arg1) throws Exception {
        ExchangeClient client = (ExchangeClient) arg0.getAttribute("client");

        if(arg1 instanceof java.io.IOException)
            client.logger.error("exception : The connexion with the host has been closed.");
        else
            client.logger.error("exception : {}", arg1);

        if(client.getServer() != null) {
            client.getServer().setState(Server.State.OFFLINE);
            Main.database.getAccountData().resetLogged(client.getServer().getId());
        }
    }

    @Override
    public void messageReceived(IoSession arg0, Object arg1) throws Exception {
        ExchangeClient client = (ExchangeClient) arg0.getAttribute("client");
        client.logger.trace("received : {}", arg1);
        IoBuffer buffer = (IoBuffer)arg1;
        while(buffer.hasRemaining())
            client.parser((IoBuffer) arg1);
    }

    @Override
    public void messageSent(IoSession arg0, Object arg1) throws Exception {
        ExchangeClient client = (ExchangeClient) arg0.getAttribute("client");
        client.logger.trace("sent : {}", arg1);
    }

    @Override
    public void sessionClosed(IoSession arg0) throws Exception {
        ExchangeClient client = (ExchangeClient) arg0.getAttribute("client");
        client.logger.debug("has been closed.");

        if(client.getServer() != null) {
            client.getServer().setState(Server.State.OFFLINE);
            Main.database.getAccountData().resetLogged(client.getServer().getId());
        }
    }

    @Override
    public void sessionCreated(IoSession arg0) throws Exception {
        arg0.setAttribute("client", new ExchangeClient(arg0));
        IoBuffer buffer = IoBuffer.allocate(20);
        buffer.clear();

        buffer.put((byte) 1);
        buffer.flip();
        arg0.write(buffer);
    }

    @Override
    public void sessionIdle(IoSession arg0, IdleStatus arg1) throws Exception {
        ExchangeClient client = (ExchangeClient) arg0.getAttribute("client");
        client.logger.debug("has been idled.");
    }

    @Override
    public void sessionOpened(IoSession arg0) throws Exception {}


}
