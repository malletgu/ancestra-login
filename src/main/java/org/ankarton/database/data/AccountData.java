package org.ankarton.database.data;

import ch.qos.logback.classic.Logger;
import com.zaxxer.hikari.HikariDataSource;
import org.ankarton.database.AbstractDAO;
import org.ankarton.kernel.Main;
import org.ankarton.object.Account;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class AccountData extends AbstractDAO<Account> {
    private final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

    public static Map<Integer, Account> accounts = new HashMap<>();

    public AccountData(HikariDataSource source) {
        super(source);
        logger = (Logger) LoggerFactory.getLogger("factory.Account");
    }

    public static Account get(int id) {
        if(accounts.get(id) != null)
            return accounts.get(id);
        return Main.database.getAccountData().load(id);
    }

    public static Account get(String name) {
        for(Account account : accounts.values())
            if(account.getName().equals(name))
                return account;
        return Main.database.getAccountData().load(name);
    }

    @Override
    public Account load(Object id) {
        Account account = null;
        try {
            String query = "SELECT * FROM accounts WHERE id = " + id;
            Result result = super.getData(query);
            account = loadFromResultSet(result.resultSet);
            close(result);
        } catch (Exception e) {
            logger.error("Can't load account with id" + id, e);
        }
        return account;
    }

    public Account load(String name) {
        Account account = null;
        try {
            String query = "SELECT * FROM accounts WHERE account = '" + name + "';";
            Result result = super.getData(query);
            account = loadFromResultSet(result.resultSet);
            close(result);
        } catch (Exception e) {
            logger.error("Can't load account with name " + name, e);
        }
        return account;
    }

    @Override
    public boolean update(Account obj) {
        try {
            String baseQuery = "UPDATE accounts SET" +
                    " pseudo = '" + obj.getPseudo() + "'," +
                    " state = '" + obj.getState() + "'," +
                    " lastConnection = '" + formatter.format(obj.getLastConnection().getTime()) + "'," +
                    " ip = '" + obj.getIp().getHostAddress() + "'," +
                    " subscribe = '" + obj.getSubscribe() + "'," +
                    " players = '" + obj.parsePlayers() + "'" +
                    " WHERE id = " + obj.getId();

            PreparedStatement statement = getPreparedStatement(baseQuery);
            execute(statement);

            return true;
        } catch (Exception e) {
            logger.error("Can't update account with id {}", obj.getId(), e);
        }
        return false;
    }

    public String getNameFromPseudo(String nickname) {
        String name = null;
        try {
            String query = "SELECT * FROM accounts WHERE pseudo = '" + nickname + "';";
            Result result = super.getData(query);
            if (result.resultSet.next())
                name = result.resultSet.getString("account");
            close(result);
            logger.debug("Account with pseudo {} exist", nickname);
        } catch (Exception e) {
            logger.error("Can't load account with pseudo like {}", nickname, e);
        }
        return name;
    }

    public boolean existPseudo(String nickname) {
        try {
            boolean exist;
            Result result = getData("SELECT id FROM accounts WHERE `pseudo`='" + nickname + "';");
            exist = result.resultSet.next();
            close(result);
            return exist;
        } catch (Exception e) {
            logger.error("Can t execute exist request", e);
        }
        return false;
    }

    protected Account loadFromResultSet(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Account account = new Account(
                    resultSet.getInt("id"),
                    resultSet.getString("account").toLowerCase(),
                    resultSet.getString("password"),
                    resultSet.getString("pseudo"),
                    resultSet.getString("question"),
                    resultSet.getString("answer"),
                    resultSet.getBoolean("level"),
                    resultSet.getShort("state"),
                    resultSet.getString("lastConnection"),
                    resultSet.getString("ip"),
                    resultSet.getLong("subscribe"),
                    resultSet.getByte("banned"),
                    resultSet.getString("players"));
            accounts.put(account.getId(), account);
            return account;
        }
        return null;
    }

    public void resetLogged(int server) {
        try {
            String baseQuery = "UPDATE accounts SET" +
                    " state = '0'" +
                    " WHERE state = " + server;
            accounts.values().forEach(a ->  a.setState((short)0,false));
            PreparedStatement statement = getPreparedStatement(baseQuery);
            execute(statement);
        } catch (Exception e) {
            logger.error("Can't update account with state {}", server, e);
        }
    }
}
