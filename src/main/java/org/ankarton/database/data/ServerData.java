package org.ankarton.database.data;

import ch.qos.logback.classic.Logger;
import com.zaxxer.hikari.HikariDataSource;
import org.ankarton.database.AbstractDAO;
import org.ankarton.object.Server;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ServerData extends AbstractDAO<Server> {

    public ServerData(HikariDataSource dataSource) {
        super(dataSource);
        logger = (Logger) LoggerFactory.getLogger("factory.Server");
    }

    public void loadAll() {
        load(null);
    }

    @Override
    public Server load(Object obj) {
        try {
            String query = "SELECT * FROM servers;";
            Result result = super.getData(query);
            ResultSet resultSet = result.resultSet;

            while (resultSet.next()) {
                new Server(resultSet.getShort("id"),
                        resultSet.getLong("key"),
                        resultSet.getByte("population"),
                        resultSet.getBoolean("isSubscriberServer"));
            }
            close(result);
            logger.info("Servers successfully loaded");
        } catch (SQLException e) {
            logger.error("Can't load server", e);
        }
        return null;
    }

    @Override
    public boolean update(Server obj) {
        return false;
    }
}
