package org.ankarton.database;

import ch.qos.logback.classic.Logger;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.ankarton.database.data.AccountData;
import org.ankarton.database.data.ServerData;
import org.ankarton.kernel.Main;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public class Database {
	//connection
	private HikariDataSource dataSource; 
    private static Logger logger = (Logger) LoggerFactory.getLogger(Database.class);
	//data
	private AccountData accountData;
	private ServerData serverData;
	
	public void initializeData() {
		this.accountData = new AccountData(dataSource);
		this.serverData = new ServerData(dataSource);
    }
	
	public void initializeConnection() {
        logger.debug("Trying to connect to database");
        HikariConfig config = new HikariConfig();

        config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        config.addDataSourceProperty("serverName", Main.databaseConfig.databaseHost());
        config.addDataSourceProperty("port", Main.databaseConfig.databasePort());
        config.addDataSourceProperty("databaseName", Main.databaseConfig.databaseName());
        config.addDataSourceProperty("user", Main.databaseConfig.databaseUser());
        config.addDataSourceProperty("password", Main.databaseConfig.databasePassword());

        dataSource = new HikariDataSource(config);
        if(!testConnection(dataSource)){
            logger.error("Please check your username and password and database connection");
            System.exit(0);
        }
        logger.info("Database connection established");
	}

	public AccountData getAccountData() { return accountData; }

	public ServerData getServerData() {
		return serverData;
	}

    private boolean testConnection(HikariDataSource dataSource) {
        try {
            Connection connection = dataSource.getConnection();
            connection.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
